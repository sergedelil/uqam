/*
 * realloc.c
 * Author : Serge D
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

const int tailleDefaut = 8;
const int tailleEtendue = 30;

int main(int argc, char** argv)
{
  char *chaine;
  char *chaine2;

  chaine = malloc(tailleDefaut * sizeof(char));
  if (!chaine)
  {
    printf("Erreur d'allocation de mémoire : %s\n", strerror(errno));
    exit(1);
  }

  strcpy(chaine, "Serge");
  printf("Chaîne : %s\n", chaine);

  chaine2 = realloc(chaine, tailleEtendue * sizeof(char));
  if (!chaine2)
  {
    printf("Erreur d'allocation de mémoire : %s\n", strerror(errno));
    free(chaine);
    exit(1);
  }

  strcpy(chaine2, "Serge Gnagnely");
  printf("Chaîne2 : %s\n", chaine2);

  free(chaine2);
  return 0;
}
