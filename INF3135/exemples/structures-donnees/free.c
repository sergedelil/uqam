/*
 * free.c
 * Author : Serge D
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main(int argc, char** argv)
{
  int* ptr;

  ptr = malloc(sizeof(int));
  if (!ptr)
  {
    printf("Erreur d'allocation de mémoire : %s\n", strerror(errno));
    exit(1);
  }

  *ptr = 42;
  printf("Valeur du pointeur : %p\nValeur de l'entier : %d\n", ptr, *ptr);

  free(ptr);
  return 0;
}
