/*
 * malloc.c
 * Author : Serge D
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main(int argc, char** argv)
{
  int *tableau;
  int taille;
  int i;

  printf("Entrez la taille du tableau : ");
  scanf("%d", &taille);

  tableau = malloc(taille * sizeof(int));
  if (!tableau)
  {
    printf("Erreur d'allocation de mémoire : %s\n", strerror(errno));
    exit(1);
  }

  for (i = 0; i < taille; i++)
  {
    tableau[i] = i;
    printf("%d\n", tableau[i]);
  }

  free(tableau);
  return 0;
}
