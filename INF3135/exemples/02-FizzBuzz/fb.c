/*
 * fb.c
 * Author : Serge D
 */
#include <stdio.h>

int main(int argc, char** argv)
{
  int max = 50;
  int i;

  for (i = 1; i < max; i++)
  {
    if (i % 3 == 0 && i % 5 == 0)
      printf("Fizz Buzz\n");
    else if (i % 3 == 0)
      printf("Fizz\n");
    else if (i % 5 == 0)
      printf("Buzz\n");
    else
      printf("%d\n", i);
  }

  return 0;
}
