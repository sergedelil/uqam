#include <stdio.h>

int main(void) {
#   define X 10
    printf("%d ", X);
#   undef X
#   define X 20
    printf("%d ", X);
    return 0;
}
