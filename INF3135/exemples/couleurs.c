#include <stdio.h>

#define RESET "\x1B[0m"
#define NOIR  "\x1B[40m"
#define ROUGE "\x1B[41m"
#define VERT  "\x1B[42m"
#define JAUNE  "\x1B[43m"
#define BLEU  "\x1B[44m"
#define ROSE  "\x1B[45m"

int main(void) {
    printf(ROUGE "rouge\n" RESET);
    printf(VERT  "vert\n"  RESET);
    printf(ROUGE  " \n"  RESET);
    printf(VERT    " \n"  RESET);
    printf(JAUNE    " \n"  RESET);
    printf(BLEU    " \n"  RESET);
    printf(ROSE    " \n"  RESET);
    return 0;
}
