#include <stdio.h>

/**
 * On réserve une matrice 10 x 10, même si on ne la remplit pas toute
 * On utilise le champ n pour se rappeler la taille réelle de la matrice
 */
struct square_matrix {
    unsigned int n;        // Matrice n x n
    double values[10][10]; // Valeurs stockées
};

/**
 * Initialise le contenu d'une matrice
 *
 * Le paramètre m est un pointeur.
 * On accède aux champs avec -> plutôt qu'avec . quand on a
 * un pointeur
 */
void initialize_matrix(struct square_matrix *m,
                       unsigned int n,
                       double v) {
    m->n = n;
    for (unsigned int i = 0; i < n; ++i)
        for (unsigned int j = 0; j < n; ++j)
            m->values[i][j] = v;
}

/**
 * On affiche de façon à aligner les valeurs, même si elles sont différentes
 * On met des crochets [ au début et ] à la fin de chaque ligne
 */
void print_matrix(const struct square_matrix *m) {
    for (unsigned int i = 0; i < m->n; ++i) {
        printf("[ ");
        for (unsigned int j = 0; j < m->n; ++j) {
            printf("%5.2lf ", m->values[i][j]);
        }
        printf("]\n");
    }
}

int main(void) {
    struct square_matrix m;
    // On passe l'adresse de m avec l'esperluette &
    initialize_matrix(&m, 8, 3.58);
    print_matrix(&m);
    return 0;
}

