#include <vec3d.h>
#include <stdio.h>

int main() {
    struct vec3d v = {1.0, 2.0, 3.0};
    printf("Testing libvec3d\n");
    printf("----------------\n");
    printf("v = ");
    vec3d_print(&v);
    printf("\n");
    printf("Square norm of v = %lf\n", vec3d_squareNorm(&v));
    printf("Norm of v = %lf\n", vec3d_norm(&v));
    printf("Is v a unit vector? %s\n",
           vec3d_isUnit(&v) ? "yes" : "no");
    printf("Normalizing v...\n");
    vec3d_normalize(&v);
    printf("v = ");
    vec3d_print(&v);
    printf("\n");
    printf("Norm of v = %lf\n", vec3d_norm(&v));
    printf("Is v a unit vector? %s\n",
           vec3d_isUnit(&v) ? "yes" : "no");
    printf("----------------\n");

    struct vec3d v1 = {-1.0, 2.0, -2.0};
    struct vec3d v2 = {3.0, -1.0, 4.0};
    printf("v1 = ");
    vec3d_print(&v1);
    printf("\n");
    printf("v2 = ");
    vec3d_print(&v2);
    printf("\n");
    double scalar = 1.5;
    printf("Multiplying v1 by %lf\n", scalar);
    vec3d_multiplyByScalar(&v1, scalar);
    printf("v1 = ");
    vec3d_print(&v1);
    printf("\n");
    printf("Adding v2 to v1\n");
    vec3d_add(&v1, &v2);
    printf("v1 = ");
    vec3d_print(&v1);
    printf("\n");
    printf("----------------\n");
    return 0;
}
