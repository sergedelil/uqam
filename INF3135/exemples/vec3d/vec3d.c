#include "vec3d.h"
#include <stdio.h>
#include <assert.h>
#include <math.h>

void vec3d_print(const struct vec3d *v) {
    printf("(%lf, %lf, %lf)", v->x, v->y, v->z);
}

void vec3d_add(struct vec3d *v1,
               const struct vec3d *v2) {
    assert(v1 != NULL);
    assert(v2 != NULL);
    v1->x += v2->x;
    v1->y += v2->y;
    v1->z += v2->z;
}

void vec3d_multiplyByScalar(struct vec3d *v,
                            double scalar) {
    assert(v != NULL);
    v->x *= scalar;
    v->y *= scalar;
    v->z *= scalar;
}

double vec3d_scalarProduct(const struct vec3d *v1,
                           const struct vec3d *v2) {
    assert(v1 != NULL);
    assert(v2 != NULL);
    return v1->x * v2->x + v1->y * v2->y + v1->z * v2->z;
}

double vec3d_squareNorm(const struct vec3d *v) {
    assert(v != NULL);
    return v->x * v->x + v->y * v->y + v->z * v->z;
}

double vec3d_norm(const struct vec3d *v) {
    return sqrt(vec3d_squareNorm(v));
}

bool vec3d_isUnit(const struct vec3d *v) {
    return vec3d_squareNorm(v) == 1.0;
}

void vec3d_normalize(struct vec3d *v) {
    double norm = vec3d_norm(v);
    v->x /= norm;
    v->y /= norm;
    v->z /= norm;
}
