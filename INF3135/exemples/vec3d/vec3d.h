#ifndef VEC3D_H
#define VEC3D_H

#include <stdbool.h>

// Types //
// ----- //

struct vec3d { // A 3D vector
    double x;  // x-coordinate
    double y;  // y-coordinate
    double z;  // z-coordinate
};

// Functions //
// --------- //

/**
 * Print the given vector to stdout
 *
 * @param v  The vector
 */
void vec3d(const struct vec3d *v);

/**
 * Add the vector `v2` to `v1`
 *
 * @param v1  The first vector
 * @param v2  The second vector
 */
void vec3d_add(struct vec3d *v1,
               const struct vec3d *v2);

/**
 * Multiply the vector `v` by `scalar`
 *
 * @param v       The vector
 * @param scalar  The scalar
 */
void vec3d_multiply_by_scalar(struct vec3d *v,
                              double scalar);

/**
 * Return the scalar product of two vectors
 *
 * If `v1 = (x1, y1, z1)` and `v2 = (x2, y2, z2)`,
 * then the scalar product of `v1` and `v2` is the
 * number `x1x2 + y1y2 + z1z2`.
 *
 * @param v1  The first vector
 * @param v2  The second vector
 * @return    The scalar product of `v1` and `v2`
 */
double vec3d_scalar_product(const struct vec3d *v1,
                            const struct vec3d *v2);

/**
 * Return the norm of `v`.
 *
 * If `v = (x, y, z)`, then the norm of `v` is the
 * number `\sqrt{x^2 + y^2 + z^2}`.
 *
 * @param v  The vector
 * @return   The norm of `v`
 */
double vec3d_norm(const struct vec3d *v);

/**
 * Return the square norm of `v`
 *
 * If `v = (x, y, z)`, then the square norm of `v`
 * is the number `x^2 + y^2 + z^2`.
 *
 * @param v  The vector
 * @return   The square norm of `v`
 */
double vec3d_square_norm(const struct vec3d *v);

/**
 * Return true if and only if `v` is a unit vector
 *
 * A unit vector is a vector whose norm is one.
 *
 * @param v  The vector
 * @return   True if and only if `v` is a unit
 */
bool vec3d_is_unit(const struct vec3d *v);

/**
 * Transform the given vector to a unit vector
 *
 * The direction is preserved, only the norm of the vector
 * is modified. This is not defined if `v = (0, 0, 0)` (in
 * that case, the vector is unchanged).
 *
 * @param v  The vector
 */
void vec3d_normalize(struct vec3d *v);

#endif
