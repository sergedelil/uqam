#include <stdio.h>

int main() {
    printf("Nom du fichier source courant: %s\n", __FILE__);
    printf("Numéro de la ligne courante: %d\n", __LINE__);
    printf("Date de compilation: %s\n", __DATE__);
    printf("Heure de compilation: %s\n", __TIME__);
    printf("Compilateur conforme à la norme ISO? %s\n",
           __STDC__ == 1 ? "oui" : "non");
    return 0;
}
