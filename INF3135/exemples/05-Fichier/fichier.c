/*
 * menu.c
 * Author : Serge D
 */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#define TAILLE_BUFFER 50

void validerArguments(int);


int main (int argc, char **argv)
{
    validerArguments(argc);
    
    FILE* fichier;
    char buffer[TAILLE_BUFFER + 1];
    
    fichier = fopen(argv[1], "r");
    if (fichier == NULL)
    {
        printf("Erreur %d\n", errno);
        return 1;
    }
    
    printf ("Contenu du fichier :\n\n");
    while (fgets(buffer, TAILLE_BUFFER, fichier) != NULL )
        printf("%s", buffer);
    
    if (fclose(fichier) == EOF)
    {
        printf ( "Erreur lors de la fermeture du fichier.\n" );
        return 1;
    }
    return 0;
}

void validerArguments(int argc)
{
    if (argc != 2)
    {
        printf("Nombre d'argument invalide \n\n");
        printf("Utilisation du programme fichier :\n fichier <chemin_fichier>\n\n");
        
        exit(1);
    }
}


