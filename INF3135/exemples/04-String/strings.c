/*
 * menu.c
 * Author : Serge D
 */
#include <stdio.h>
#include <string.h>

#define TAILLE_NOM 40

int main(int argc, char **argv)
{
  char *chaineStatique = "Universite du Quebec a Montreal";
  char buffer[TAILLE_NOM + 1];
  char *pointeur;
  char *pointeur2;
  
  printf("La taille de la chaine statique : %lu\n", strlen(chaineStatique));

  // Construire une chaîne, caractère par caractère
  buffer[0] = 'A';
  buffer[1] = 'l';
  buffer[2] = 'l';
  buffer[3] = 'o';
  buffer[4] = '\0';
  printf("Chaine construite caractere par caractere : %s\n", buffer);

  // Positionner un pointeur sur une partie de la chaîne (M)
  pointeur = chaineStatique + 23;
  printf("Fraction de chaine par pointeur : %s\n", pointeur);

  // Copie
  strcpy(buffer, pointeur);
  printf("Copie : %s\n", buffer);

  // Concaténation
  strcat(buffer, ", ");
  pointeur2 = chaineStatique + 14; //Q
  strncat(buffer, pointeur2, 6); // Montreal, Quebec
  printf("Resultat de concatenations : %s\n", buffer);

  // Comparaison
  if (strcmp(buffer, "Montreal, Quebec") == 0)
    printf("Chaines egales\n");

  return 0;
}
