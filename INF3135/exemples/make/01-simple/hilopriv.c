/*
 * hilopriv.c
 * Author : Serge D
 */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "hilopriv.h"

int obtenirNombreAleatoire()
{
  srand(time(NULL));
  return rand() % 100;
}

int saisirNombre(int coupsRestants)
{
  int valeur;
  char* s = coupsRestants > 1 ? "s" : "";
  printf("Entrez une valeur supérieure ou égale à 0 et inférieure à 100 (%d coup%s restant%s) : ",
         coupsRestants, s, s);
  scanf("%d", &valeur);
  return valeur;
}
