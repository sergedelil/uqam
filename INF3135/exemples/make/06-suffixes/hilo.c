/*
 * hilo.c
 * Author : Serge D
 */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "hilo.h"
#include "hilopriv.h"

void hilo()
{
  int cible, saisie;
  int coups = 10;
  enum Bool enJeu = VRAI;

  cible = obtenirNombreAleatoire();
  do
  {
    saisie = saisirNombre(coups);
    coups--;
    if (saisie == cible)
    {
      enJeu = FAUX;
      printf("Bravo, vous avez trouvé le nombre mystère : %d\n\n", cible);
    }
    else if (coups == 0)
    {
      enJeu = FAUX;
      printf("Vous avez perdu. La cible était : %d\n\n", cible);
    }
    else if (saisie < cible)
      printf("Plus haut\n");
    else
      printf("Plus bas\n");
  } while (enJeu);
}
