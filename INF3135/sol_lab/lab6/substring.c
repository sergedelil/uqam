#include <stdio.h>
#include <string.h>

#define USAGE "\
Usage: %s WORD [-o FILENAME]\n\
Prints the Hasse diagram of WORD to the DOT format\n\
with respect to the subword relation.\n\
By default, prints to stdout.\n\
\n\
Option:\n\
  -o FILENAME    Writes output to FILENAME\n\
"

/**
 * Prints the given subword
 *
 * @param file  The stream on which to print
 * @param s     The reference word
 * @param i     The start index of the subword
 * @param n     The length of the subword
 */
void print_subword(FILE *file,
                   const char *word,
                   unsigned int i,
                   unsigned int n) {
    if (n == 0)
        fprintf(file, "empty");
    else
        fprintf(file, "%.*s", n, word + i);
}

/**
 * Prints all Graphviz nodes
 *
 * @param file  The stream on which to print
 * @param word  The reference word
 */
void print_all_nodes(FILE *file, const char *word) {
    unsigned int nmax = strlen(word);
    for (unsigned int n = 0; n <= nmax; ++n) {
        fprintf(file, "    subgraph cluster_%d {\n", n);
        fprintf(file, "        color=%d;\n", 1 + n % 9);
        for (unsigned int i = 0; i < nmax + 1 - n; ++i) {
            fprintf(file, "        ");
            print_subword(file, word, i, n);
            fprintf(file, ";\n");
        }
        fprintf(file, "    }\n");
    }
}

/**
 * Prints the prefix-suffix-to-subword edges
 *
 * @param file  The stream on which to print
 * @param word  The reference word
 * @param i     The start index of the subword
 * @param n     The length of the subword
 */
void print_edges_pair(FILE *file,
                      const char *word,
                      unsigned int i,
                      unsigned int n) {
    fprintf(file, "    ");
    print_subword(file, word, i, n - 1);
    fprintf(file, " -> ");
    print_subword(file, word, i, n);
    fprintf(file, ";\n");
    fprintf(file, "    ");
    print_subword(file, word, i + 1, n - 1);
    fprintf(file, " -> ");
    print_subword(file, word, i, n);
    fprintf(file, ";\n");
}

/**
 * Prints all Graphviz edges
 *
 * @param file  The stream on which to print
 * @param word  The reference word
 */
void print_all_edges(FILE *file,
                     const char *word) {
    unsigned int nmax = strlen(word);
    for (unsigned int n = 1; n <= nmax; ++n)
        for (unsigned int i = 0; i < nmax + 1 - n; ++i)
            print_edges_pair(file, word, i, n);
}

/**
 * Prints all Graphviz edges
 *
 * @param file  The stream on which to print
 * @param word  The reference word
 */
void print_graph(FILE *file, const char *word) {
    fprintf(file, "strict digraph {\n");
    fprintf(file, "    style=filled; labeljust=l; colorscheme=rdylbu9\n");
    fprintf(file, "    node [shape=plain];\n");
    fprintf(file, "    edge [arrowhead=none];\n");
    fprintf(file, "    empty [label=<&#949;>];\n");
    print_all_nodes(file, word);
    print_all_edges(file, word);
    fprintf(file, "}\n");
}

int main(int argc, char *argv[]) {
    if (argc == 2) {
        print_graph(stdout, argv[1]);
        return 0;
    } else if (argc == 4 && strcmp(argv[2], "-o") == 0) {
        FILE *file;
        if ((file = fopen(argv[3], "w"))) {
            print_graph(file, argv[1]);
            fclose(file);
            return 0;
        } else {
            fprintf(stderr, "Error: cannot write to %s\n", argv[3]);
            fprintf(stderr, USAGE, argv[0]);
            return 1;
        }
    } else {
        fprintf(stderr, USAGE, argv[0]);
        return 1;
    }
}
