#include <stdio.h>

// Couleur d'une pièce
enum couleur {
    NOIR,
    BLANC
};

// Type de pièce
enum type_piece {
    PION,
    TOUR,
    CAVALIER,
    FOU,
    DAME,
    ROI
};

// Pièce
struct piece {
    enum couleur couleur;
    enum type_piece type_piece;
};

// Échiquier
struct echiquier {
    struct piece pieces[32];   // Les 32 pièces possibles
    struct piece *cases[8][8]; // Pour chaque case, la pièce qui l'occupe
    enum couleur joueur;       // Le joueur qui doit jouer
};

/**
 * Ajoute une pièce sur un échiquier
 *
 * @param echiquier   L'échiquier
 * @param p           Le numéro de la pièce
 * @param r           La ligne où placer la pièce
 * @param c           La colonne où placer la pièce
 * @param couleur     La couleur de la pièce
 * @param type_piece  Le type de pièce
 */
void ajouter_piece(struct echiquier *echiquier,
                   unsigned int p,
                   unsigned int r,
                   unsigned int c,
                   enum couleur couleur,
                   enum type_piece type_piece) {
    echiquier->pieces[p] = (struct piece){couleur, type_piece};
    echiquier->cases[r][c] = &echiquier->pieces[p];
}

/**
 * Initialise l'échiquier
 *
 * @param echiquier  L'échiquier à initialiser
 */
void initialiser_echiquier(struct echiquier *echiquier) {
    for (unsigned int r = 0; r < 8; ++r)
        for (unsigned int c = 0; c < 8; ++c)
            echiquier->cases[r][c] = NULL;
    // Pions
    for (unsigned int i = 0; i < 8; ++i) {
        ajouter_piece(echiquier, i, 6, i, BLANC, PION);
        ajouter_piece(echiquier, i + 16, 1, i, NOIR, PION);
    }
    // Tours
    ajouter_piece(echiquier, 8, 7, 0, BLANC, TOUR);
    ajouter_piece(echiquier, 9, 7, 7, BLANC, TOUR);
    ajouter_piece(echiquier, 24, 0, 0, NOIR, TOUR);
    ajouter_piece(echiquier, 25, 0, 7, NOIR, TOUR);
    // Cavaliers
    ajouter_piece(echiquier, 10, 7, 1, BLANC, CAVALIER);
    ajouter_piece(echiquier, 11, 7, 6, BLANC, CAVALIER);
    ajouter_piece(echiquier, 26, 0, 1, NOIR, CAVALIER);
    ajouter_piece(echiquier, 27, 0, 6, NOIR, CAVALIER);
    // Fous
    ajouter_piece(echiquier, 12, 7, 2, BLANC, FOU);
    ajouter_piece(echiquier, 13, 7, 5, BLANC, FOU);
    ajouter_piece(echiquier, 28, 0, 2, NOIR, FOU);
    ajouter_piece(echiquier, 29, 0, 5, NOIR, FOU);
    // Dames
    ajouter_piece(echiquier, 14, 7, 3, BLANC, DAME);
    ajouter_piece(echiquier, 30, 0, 3, NOIR, DAME);
    // Rois
    ajouter_piece(echiquier, 15, 7, 4, BLANC, ROI);
    ajouter_piece(echiquier, 31, 0, 4, NOIR, ROI);
    // Blanc commence
    echiquier->joueur = BLANC;
}

/**
 * Affiche le type d'une pièce sur stdout
 *
 * @param t  Le type de pièce
 */
void afficher_type_piece(enum type_piece t) {
    switch (t) {
        case PION:     putchar('P'); break;
        case TOUR:     putchar('T'); break;
        case CAVALIER: putchar('C'); break;
        case FOU:      putchar('F'); break;
        case DAME:     putchar('D'); break;
        case ROI:      putchar('R'); break;
    }
}

/**
 * Affiche une couleur sur stdout
 *
 * @param c  La couleur
 */
void afficher_couleur(enum couleur c) {
    switch (c) {
        case BLANC: putchar('B'); break;
        case NOIR:  putchar('N'); break;
    }
}

/**
 * Affiche une pièce sur stdout
 *
 * @param piece  La pièce à afficher
 */
void afficher_piece(const struct piece *piece) {
    afficher_type_piece(piece->type_piece);
    afficher_couleur(piece->couleur);
}

/**
 * Affiche un échiquier sur stdout
 *
 * @param echiquier  L'échiquier à afficher
 */
void afficher_echiquier(const struct echiquier *echiquier) {
    for (unsigned int r = 0; r <= 16; ++r) {
        if (r % 2 == 0)
            printf("+");
        else
            printf("|");
        for (unsigned int c = 0; c < 16; ++c) {
            if (r % 2 == 0 && c % 2 == 1)
                printf("--+");
            else if (r % 2 == 1 && c % 2 == 0) {
                struct piece *piece = echiquier->cases[r/2][c/2];
                if (piece != NULL)
                    afficher_piece(piece);
                else
                    printf("  ");
                printf("|");
            }
        }
        printf("\n");
    }
    printf("Le trait aux %s\n",
            echiquier->joueur == BLANC ? "blancs" : "noirs");
}

int main(void) {
    struct echiquier echiquier;
    initialiser_echiquier(&echiquier);
    afficher_echiquier(&echiquier);
    return 0;
}
