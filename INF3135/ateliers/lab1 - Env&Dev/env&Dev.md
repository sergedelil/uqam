# Labo 1: Environnement de travail

## 1 - Installation d'un système Unix (au moins 60 minutes)

*Note:* cet exercice n'est pas abordé en laboratoire et devrait être complété
à la maison. Prévoir plusieurs heures pour l'installation et la configuration
si c'est la première fois que vous faites cela.

Si vous avez un ordinateur portable et que vous souhaitez l'utiliser pour
développer vos travaux pratiques, il est important d'installer le plus
rapidement possible un système Unix sur votre machine. Bien qu'il soit possible
de travailler sous MacOS, ce sera Linux (Ubuntu) qui sera supporté et utilisé.
Voici les environnements recommandés dans le cadre de ce cours:

* Utilisateurs *Linux*. C'est l'environnement recommandé.

* Utilisateurs *MacOS*. Assurez-vous d'avoir installé
  [XCode](https://developer.apple.com/xcode/). Vous devez aussi installer un
  gestionnaire de paquets, qui simplifiera l'installation de programmes tout au
  long du cours. Les deux gestionnaires les plus utilisés sont
  [MacPorts](https://www.macports.org/) (plus ancien) et
  [Homebrew](https://brew.sh/index_fr.html) (plus moderne). Il n'est pas
  recommandé d'installer les deux, car ils ne coexistent pas bien ensemble et
  cela peut entraîner des conflits. **Attention**: plusieurs commandes et
  configurations particulières à MacOS peuvent entraîner des comportements
  différents de ceux obtenus dans le cadre du cours.

* Utilisateurs *Windows*. Il est fortement recommandé d'installer un système
  Linux sur votre machine personnelle. En particulier, vous ne pourrez pas
  compléter le TP2 ou le TP3 sur le serveur Java, qui n'offre pas d'interface
  graphique. Il y a donc deux possibilités qui s'offrent à vous:

  - (**recommandé**) Installer un système Linux en partition double. Cette
    solution est préférable à la suivante si vous souhaitez avoir une meilleure
    expérience et moins de problèmes techniques à résoudre (configuration,
    délais, utilisation sous-optimale du matériel). S'il s'agit de votre
    première expérience Linux, il est recommandé d'utiliser
    [Ubuntu](https://www.ubuntu.com/) ou [Linux
    Mint](https://www.linuxmint.com/)
  - (**possible**) Installer une machine virtuelle Linux. Voir par exemple [la
    capsule du CLibre](https://www.youtube.com/watch?v=1zfO-Fhqyb8) sur le
    sujet.

Le [CLibre](http://clibre.uqam.ca/) est une organisation qui relève de la
Faculté des sciences et dont le mandat est de promouvoir le logiciel libre.
N'hésitez pas à les contacter si vous avez besoin d'aide pour cette étape

## 2 - Établir une connexion SSH (10 minutes)

Des comptes étudiants ont été créés pour chacun d'entre vous sur le serveur
Java (`java.labunix.uqam.ca`). Pour vous y connecter, vous devez connaître
votre [identifiant MS](https://etudier.uqam.ca/code-dacces-ms) (de la forme
`ab123456`) ainsi que votre mot de passe (de la forme `ABC12345`).

Générez une paire de clés SSH (si vous ne l'avez pas déjà fait dans un autre
cours). Sur Linux, il suffit de lancer la commande
[ssh-keygen](https://www.ssh.com/ssh/keygen/) et de suivre les instructions.
Par défaut, ces clés sont stockées dans votre répertoire personnel:

* Clé **publique**: `~/.ssh/id_rsa.pub`
* Clé **privée**: `~/.ssh/id_rsa`

Familiarisez-vous avec les étapes de base pour établir une connexion SSH au
serveur Java. Il s'agit d'une partie très importante, car votre premier travail
pratique devra minimalement fonctionner sur le serveur Java. Parfois, certains
comptes étudiants ne sont pas activés sur les serveurs. Si vous avez des
problèmes de connexion, contactez votre enseignant le plus rapidement possible
pour qu'il rectifie la situation.

## 3 - Vim (30 minutes)

Complétez le tutoriel intégré de Vim. Pour cela, il suffit d'ouvrir un terminal
et de taper la commande suivante (le caractère `$` est utilisé pour désigner
l'invite):

```sh
$ vimtutor
```

Puis suivez les instructions.

Ensuite, récupérez le fichier [vimrc](../config/vimrc) disponible dans le
répertoire [config](../config) et placez-le sur votre machine dans
`~/.vim/vimrc`. Notez que [Vim-plug](https://github.com/junegunn/vim-plug) est
installé automatiquement lors du premier lancement de Vim avec cette
configuration, et s'occupe d'installer les différents plugins spécifiés dans
celle-ci. Vous aurez alors une configuration minimale pour travailler
efficacement sous Vim pendant tout le cours.

## 4 - Git et GitLab (45 minutes)

Le prochain exercice vise à vous familiariser avec le logiciel
[Git](https://git-scm.com/) et la plateforme [GitLab du
département](https://gitlab.info.uqam.ca/).

* Examinez le fichier [gitconfig](../config/gitconfig). Si vous avez déjà
  configuré Git sur votre machine, il est tout de même encouragé d'ajouter
  certains éléments, notamment les *alias*, qui seront souvent utilisés. Si
  vous n'avez jamais configuré Git sur votre machine, copiez le contenu du
  fichier et placez-le dans `~/.gitconfig` (n'oubliez pas le `.` au début).
  Puis, remplacez `<Votre nom complet>` par votre nom et `<Votre courriel>` par
  votre courriel (UQAM de préférence, ou personnel). Ne laissez pas les
  symboles `<` et `>`.

* Rendez-vous sur le [GitLab du
  cours](https://gitlab.info.uqam.ca/) et authentifiez-vous
  à l'aide de votre code MS et de votre mot de passe. Familiarisez-vous avec
  l'interface web.

* Dans les paramètres de votre compte, ajoutez votre clé SSH publique
  (`id_rsa.pub`).

* À l'aide de la commande `git clone`, clonez le dépôt qui contient les
  laboratoires sur votre machine. Au besoin, consultez l'aide avec `git clone
  --help`. Vous pourrez compléter les laboratoires directement dans ce
  répertoire pendant toute la session si vous le souhaitez.

* Sur le GitLab du département d'informatique, créez un dépôt nommé
  `inf3135-solution-labos`. Vous pouvez laisser la description vide et ne pas
  initialiser le dépôt avec un fichier `README.md`.

* Sur votre machine, créez un répertoire nommé `solution-labos`. Établissez le
  lien entre votre projet GitLab et votre répertoire local à l'aide de la
  commande `git remote`. Consultez l'aide avec `git remote --help`. Portez une
  attention aux sous-commandes `add`, `rm`, `set-url` et à l'option `-v` de la
  commande `git remote`, qui sont particulièrement utiles.

* Créez un sous-répertoire `labo1` à l'intérieur de votre répertoire
  `solution-labos`, puis déplacez-vous dans le sous-répertoire nouvellement
  créé.

## 5 - Compilation d'un programme C (20 minutes)

Le but de cet exercice est de vous familiariser avec la compilation de base
d'un fichier C.

* Créez un programme C nommé `hello.c` qui affiche le traditionnel « Hello,
  world! » et sauvegardez-le. Vous pouvez simplement copier les lignes
  suivantes:

    ```c
    #include <stdio.h>

    int main(int argc, char *argv[]) {
        printf("Hello, world!\n");
        return 0;
    }
    ```

* Compilez le fichier en une seule étape et exécutez-le

    ```sh
    $ gcc hello.c
    $ ./a.out
    ```

* Utilisez l'option `-o` de `gcc` pour produire un exécutable nommé `hello`,
  puis exécutez le programme.

* Lancez les commandes suivantes. Qu'observez-vous?

    ```sh
    $ gcc -c hello.c
    $ ls
    ```

* À l'aide de la commande `git add`, ajoutez le fichier `hello.c`, puis
  sauvegardez votre projet Git avec `git commit` (ou `git ci`). Écrivez un
  message de *commit* qui respecte le [format
  recommandé](https://chris.beams.io/posts/git-commit/), en français ou en
  anglais.

* Regardez l'état de votre projet avec `git status` (ou `git st`). Que
  remarquez-vous?

* Créez un fichier `.gitignore` et ajoutez des lignes permettant d'ignorer les
  fichiers avec les extensions `.o` et `.out`, ainsi que l'exécutable. Puis
  sauvegardez-le avec `git add` et `git commit`.

* À l'aide de la commande `rm`, supprimez les fichiers binaires générés par les
  commandes précédentes.

## 6 - Markdown (30 minutes)

Vous allez maintenant créer un fichier `README` au format Markdown pour
documenter votre projet.

* Dans le même répertoire que le fichier `hello.c` que vous avez conçu
  à l'exercice précédent, créez un fichier nommé `README.md` qui décrit
  brièvement votre projet.

* Indiquez minimalement le titre du projet, l'auteur du projet et une ou deux
  phrases qui décrivent ce qu'il fait, ainsi qu'une section qui décrit tous les
  fichiers présents dans le répertoire. Profitez-en pour explorer la [syntaxe
  Markdown spécifique à GitLab](https://docs.gitlab.com/ee/user/markdown.html). 

* Ajoutez un extrait de code C multiligne (à l'aide de `` ```c``).

* Expliquez comment compiler le programme en donnant explicitement la commande
  (à l'aide de `` ```sh``).

* Puis, utilisez [Pandoc](https://pandoc.org/) pour transformer votre fichier
  `README.md` au format PDF et au format HTML localement. Visualisez le
  résultat et assurez-vous que le rendu est bien formaté.

* Transformez à nouveau votre fichier `README.md` au format HTML en utilisant
  le fichier de style [github-pandoc.css](../misc/github-pandoc.css). Les
  options `-c`, `-s` et `-o` de Pandoc vous seront utiles.

* Versionnez votre fichier `README` (et tout autre fichier pertinent) à l'aide
  de `git add` et `git commit`.

* Finalement, poussez votre code sur votre dépôt GitLab à l'aide de la commande

    ```sh
    $ git push origin master
    ```

* Vérifiez le résultat sur la plateforme web. En particulier, assurez-vous que
  la version HTML de votre fichier `README.md` est bien formatée.
