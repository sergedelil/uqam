# Labo 6: Entrées et sorties

L'objectif de ce laboratoire est de vous familiariser avec les éléments
suivants:

* Les fichiers en C
* Les canaux standards
* Le programme [Graphviz](https://graphviz.org/)
* Le programme [QPDF](http://qpdf.sourceforge.net/)

## 1 - Génération d'un diagramme de Hasse (30 minutes)

* Récupérez le fichier [`substring.c`](substring.c) disponible dans ce
  répertoire
* Complétez les fonctions de telle sorte que votre programme permette
  d'afficher sur `stdout` du texte au format DOT décrivant le [diagramme de
  Hasse](https://fr.wikipedia.org/wiki/Diagramme_de_Hasse) de la relation
  « être une sous-chaîne de ». Utilisez la clé `empty` pour identifier la
  chaîne vide. Ne vous préoccupez pas des doublons (ils seront gérés par
  Graphviz). À la fin, vous devriez obtenir un résultat semblable à:

```sh
$ gcc substring.c -o substring
$ ./substring aab
strict digraph {
    empty;
    empty;
    empty;
    empty;
    a;
    a;
    b;
    aa;
    ab;
    aab;
    empty -> a;
    empty -> a;
    empty -> a;
    empty -> a;
    empty -> b;
    empty -> b;
    a -> aa;
    a -> aa;
    a -> ab;
    b -> ab;
    aa -> aab;
    ab -> aab;
}
```

* Vérifiez que le graphe produit par Graphviz est correct:

```sh
$ ./substring aab | dot -Tpdf -o substring.pdf
```

*Remarque*:

* Soit `n` la longueur de la chaîne à traiter. Pour énumérer tous les arcs, il
  suffit d'énumérer chacune des sous-chaînes de longueur `1` à `n`: pour chaque
  sous-chaîne `s`, on ajoute alors les arcs de la forme `s[0..n-1] -> s` et
  `s[1..n] -> s`
* En Graphviz, on indiquer qu'un graphe orienté (*digraph*) est *strict* si on
  souhaite fusionner les arcs multiples en un seul arc.

## 2 - Regroupement en clusters et stylisation (20 minutes)

Modifiez le programme précédent de façon à améliorer la présentation:

* Regroupez les sous-chaînes de même longueur en *clusters* 
* Ajoutez de la couleur aux *clusters* en utilisant une palette de couleur
  (`colorscheme=rdylbu9`). La palette de couleur vous permet d'utiliser des
  numéros de couleurs (`color=1`, `color=2`, etc.) plutôt que de mettre des
  noms de couleur explicites.
* Supprimez l'ellipse autour des sous-chaînes (`shape=plain`)
* Supprimez la flèche des arcs (`arrowhead=none`)
* Remplacez la chaîne `empty` par la variable grecque *epsilon*, qui désigne la
  chaîne vide

À la fin, vous devriez obtenir quelque chose qui ressemble à ceci:

```sh
$ gcc substring.c -o substring
$ ./substring aab
strict digraph {
    style=filled; colorscheme=rdylbu9
    node [shape=plain];
    edge [arrowhead=none];
    empty [label=<&#949;>];
    subgraph cluster_0 {
        color=1;
        empty;
        empty;
        empty;
        empty;
    }
    subgraph cluster_1 {
        color=2;
        a;
        a;
        b;
    }
    subgraph cluster_2 {
        color=3;
        aa;
        ab;
    }
    subgraph cluster_3 {
        color=4;
        aab;
    }
    empty -> a;
    empty -> a;
    empty -> a;
    empty -> a;
    empty -> b;
    empty -> b;
    a -> aa;
    a -> aa;
    a -> ab;
    b -> ab;
    aa -> aab;
    ab -> aab;
}
```

* Vérifiez le résultat à l'aide de la commande `dot`:

```sh
$ ./substring aab | dot -Tpdf -o substring.pdf
```

## 3 - Écriture dans un fichier (40 minutes)

* Modifiez le programme précédent afin qu'il supporte l'option `-o` décrite
  dans le manuel d'utilisation. Plus précisément, cette option permet d'écrire
  le texte au format DOT dans un fichier plutôt que sur la sortie standard
  (`stdout`). N'hésitez pas à modifier la signature de vos fonctions en
  ajoutant un paramètre de type `FILE*`. À la fin, on s'attend au résultat
  suivant:

```sh
$ ./substring aab -o substring.dot
$ cat substring.dot
strict digraph {
    style=filled; colorscheme=rdylbu9
    node [shape=plain];
    edge [arrowhead=none];
    empty [label=<&#949;>];
[...]
```

* Ensuite, traitez les différents cas d'erreur possibles: arguments incorrects,
  impossible d'ouvrir le fichier, etc. Retournez la valeur `1` en cas d'erreur
  et affichez des messages d'erreur sur `stderr`.

## 4 - Écriture d'un script shell (30 minutes)

Écrivez un script shell nommé `hasse` qui produit les diagrammes de Hasse des
chaînes:

* `abracadabra`
* `ababababab`
* `abaababaabaab`
* `patate`
* `elephant`

et qui les stockent dans un seul fichier PDF.

*Remarques*:

* Un script shell est un fichier texte qui contient une suite de commandes
  qu'on pourrait entrer en ligne de commandes
* La première ligne devrait être `#!/bin/sh` pour indiquer qu'il s'agit d'un
  script shell
* On doit le rendre exécutable avec la commande `chmod`:

```sh
$ chmod +x hasse
```

* La commande `qpdf` permet de manipuler des fichiers pdf et, en particulier,
  de concaténer plusieurs pages
* Pour ne pas polluer votre répertoire avec des fichiers temporaires, vous
  pouvez utiliser la commande `mktemp`, qui permet de générer des fichiers et
  des répertoire temporaires. Par exemple, `tmpdir=$(mktemp -d)` génère un
  répertoire temporaire et sauvegarde le nom de ce répertoire dans `tmpdir`.
