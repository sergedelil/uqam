# Labo 6: Modules

## 1 - Le module `treemap` (60 minutes)

### A - Installation

Si ce n'est pas déjà fait...

* Installez la bibliothèque Libtap sur votre machine personnelle.
  L'installation doit être faite manuellement:
* Clonez le dépôt
  [https://github.com/zorgnax/libtap](https://github.com/zorgnax/libtap) dans
  un répertoire quelconque. Moi, j'utilise `~/Applications/libtap`.
* Rendez-vous dans ce répertoire: `cd ~/Applications/libtap`.
* Faites `make`.
* Puis `sudo make install`.
* Faites également `ldconfig` (ou `sudo ldconfig`). Si `ldconfig` n'est pas
  installé, sur Debian et ses dérivées (dont Ubuntu), il suffit de faire `sudo
  apt install libc-bin`.
* Finalement, installez
  [PKG-config](https://www.freedesktop.org/wiki/Software/pkg-config/). Sur
  Debian et dérivées, il suffit d'entrer `sudo apt install pkg-config`.

### B - Récupération du module `treemap`

* Récupérez le fichier
  [`treemap.c`](treemap.c) disponible sur ce dépôt Git.
* Compilez-le et exécutez-le. Assurez-vous qu'il fonctionne bien.
* Vérifiez avec Valgrind s'il y a des fuites mémoire. Corrigez-les le cas
  échéant.

### C - Transformation en module

* Répartissez le code du fichier `treemap.c` dans trois fichiers: `treemap.h`
  (l'interface), `treemap.c` (l'implémentation) et `test_treemap.c` (qui
  utilise et teste le module `treemap`).
* Ajoutez un Makefile qui permet de compiler séparément les deux nouveaux
  modules (`treemap` et `test_treemap`), et qui fait aussi l'édition des liens
  séparément. Utilisez des règles *explicites*. Vous devriez en avoir 3,
  produisant respectivement les fichiers `treemap.o`, `test_treemap.o` et
  `test_treemap` (l'exécutable).
* Ajoutez une règle `clean` qui supprime les fichiers générés.

### D - Tests

* Modifiez le fichier `test_treemap.c` pour utiliser Libtap.
* Transformez les tests déjà présents pour exploiter les fonctionnalités de
  Libtap. N'hésitez pas à modifier le module `treemap` pour mieux tester. Par
  exemple, vous pourriez ajouter un paramètre `prefix` à la fonction
  `treemap_print` pour que l'affichage soit compatible avec le protocole TAP
  (les commentaires de diagnostic devraient commencer par `#`)
* Mettez à jour le Makefile pour qu'il puisse compiler avec Libtap. PKG-config
  vous sera utile. Ajoutez une cible `test` qui permet d'exécuter les tests de
  façon automatique. (N'oubliez pas la dépendance de la cible `test`
  à l'exécutable.)
* N'hésitez pas à ajouter des tests supplémentaires au besoin.
* Vérifiez bien que le code de retour de `make test` est `0` quand tous les
  tests réussissent et qu'il est différent de `0` dans le cas contraire.

## 2 - La bibliothèque Cairo (60 minutes)

Si ce n'est pas encore fait, installez la bibliothèque Cairo sur votre machine
personnelle. La façon de faire varie selon la distribution Linux ou Mac.

Puis, écrivez un petit programme nommé `grille.c` qui génère une grille `m
x n`, avec un décalage de 20 pixels entre chaque ligne, de couleur noire, sur
un arrière-plan blanc, et qui la sauvegarde dans un fichier nommé `grille.png`.
Par exemple, si vous entrez la commande

```sh
$ ./grille 5 4 20
```

alors vous devriez obtenir un fichier PNG contenant une grille de 5 lignes et
4 colonnes, avec un espacement de 20 pixels entre les lignes (autrement dit,
chaque petit carré est de dimension 20 par 20 pixels), tel qu'illustré ci-bas:

![](grille.png)

Les fonctions suivantes pourraient vous être utiles:

* `cairo_image_surface_create`: pour créer une surface sur laquelle dessiner
* `cairo_create`: pour créer un contexte Cairo
* `cairo_set_source_rgb`: pour choisir la couleur du crayon
* `cairo_rectangle`: pour créer un chemin rectangulaire
* `cairo_fill`: pour colorier la région délimitée par un chemin
* `cairo_move_to`: pour se déplacer à un point donné
* `cairo_line_to`: pour ajouter un segment dans le chemin courant
* `cairo_stroke`: pour tracer le chemin courant
* `cairo_surface_write_to_png`: pour sauvegarder une image dans un fichier
* `cairo_destroy`: pour détruire un contexte Cairo
* `cairo_surface_destroy`: pour détruire une surface Cairo

*Remarques*:

* Afin de gagner du temps, vous n'avez pas à valider les arguments.
* Un court tutoriel introductif à Cairo est disponible
  [ici](https://www.cairographics.org/tutorial/)
* Le manuel de référence de Cairo est disponible
  [ici](https://www.cairographics.org/manual/)
