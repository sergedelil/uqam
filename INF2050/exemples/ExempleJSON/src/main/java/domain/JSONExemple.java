package domain;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class JSONExemple {

    private String filename;
    private String outputFile;
    private JSONObject jsonObj;

    public JSONExemple(String filename, String outputFile){

        this.filename = filename;
        this.outputFile = outputFile;
        this.jsonObj = null;
    }

    public void load() throws IOException, ParseException {

        Object obj = new JSONParser().parse(new FileReader(filename));
        this.jsonObj = (JSONObject) obj;
    }

    public void save() throws FileNotFoundException {

        PrintWriter pw = new PrintWriter(outputFile);
        pw.write(jsonObj.toJSONString());

        pw.flush();
        pw.close();
    }

    public void to_string() {
        String client = (String) jsonObj.get("client");
        System.out.println("client : "+ client);

        String contrat = (String) jsonObj.get("contrat");
        System.out.println("contrat : "+ contrat);

        String mois = (String) jsonObj.get("mois");
        System.out.println("mois : "+ mois);

        JSONArray jsonArray = (JSONArray) jsonObj.get("reclamations");
        for (Object arrayObj : jsonArray) {
            JSONObject reclamation = (JSONObject) arrayObj;
            System.out.println("===========================");
            System.out.println("soin : " + reclamation.get("soin"));
            System.out.println("date : " + reclamation.get("date"));
            System.out.println("montant : " + reclamation.get("montant"));
        }
    }
}
