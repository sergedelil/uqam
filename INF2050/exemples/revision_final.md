## Révision examen final  - INF2050

---

###### FORMAT EXAMEN INTRA

* 5 questions à développement (10 points/question) : 50 points
* 25 questions QCM (2points/question)              : 50 points


----

# Partie 1

### Gestion de sources - partie 2 (branches)

* Une branche est une référence mutable alors que le tag est une référence immuable 

* Le HEAD est une référence qui pointe vers une autre référence. Il peut aussi directement vers un commit. On dit qu'il est détaché.

* ```git branch <branche>``` crée la branche
* ```git checkout <branche>``` s'y déplace ou encore```git checkout -b <branche>``` crée une nouvelle branche et déplace HEAD vers cette nouvelle branche

* ```git merge <branche>``` copie les modifications de la branche ```<branche>``` dans la branche courante

* ```git branch -d <branche>``` supprimer la branche

* ```git cherry-pick <hash> ``` copie un commit d’une branche à une autre

* ```git push --set-upstream origin <branche>  ``` envoie une branche créé localement sur le dépôt central

* Un Pull Request ou un Merge Request est une demande de merge (de fusion) faite à travers l’interface de l’hébergeur.
  

###  Normes de codification

###### But

* Uniformité du style
* Cette uniformité favorise la lisibilité
* Et la lisibilité favorise la maintenabilité


Le style est la mise en forme des différents éléments syntaxiques (position des accolades, …) et la mise en forme de la documentation dans le code

###### les éléments de styles : 
- La position des accolades
- Les endroits où l’on place des espaces (dans les parenthèses, autour des opérateurs, après les mots-clés, etc.)
- L'indentation (nombre d’espaces, tabulations)
- La longueur des lignes de code
- La position des déclarations des variables
- La mise en page des commentaires 
- L'ordre d'apparition des éléments dans un fichier  ou une classe
- La nomenclature est l'élément le plus important du style. Il touche fortement la lisibilité du code

###### La casse en java :
- camelCase (variables et méthodes)
- PascalCase ou uper camelCase (équivalent) pour les nom de classe
- Snake_case (plus en python) pour des constantes java mais en majustcule (uper snake_case)
- Hyphen-case (plus dans le web : classe css, URL, paramètre d’URL, ..)

- Et bien sûr garder en vue toutes les discussions qu'on a eu classe sur le sujet.


### Tests unitaires 2

###### Fake et Mock Object

* Fake Object : Un faux objet qui se fait passer pour un autre objet dont nous dépendons
* Les méthodes du fake retourneront habituellement des valeurs hard-coded

* Mock Object : Une forme plus évoluée d'un fake object dans lequel le faux objet ajoute de la fonctionnalité de test (Un fake object avec des attentes).


Autrement dit :
* Au niveau fake : on décrit la méthode
* Au niveau mock : on s’assure que cette méthode est bien appelée (assert possible)

* Les Stubs et Drivers permettent aux composants d’être isolés du reste du système pour être testés
* Les Stubs pour maîtriser les dépendances des modules de bas niveau
* Les Drivers pour maîtriser les dépendances des modules de haut niveau

###### Injection de dépendance

* Si un objet A dépend d’un autre objet B, sortir B et l’injecter à A à travers le constructeur de A.

###### Desing for Testability

* Faire la conception de façon à favoriser l'écriture des tests

###### Test de caractérisation

* Technique pour mettre sous test du code  particulièrement difficile à travailler.
* Utile pour les longues méthodes, les méthodes très complexes ou qu'on ne s'y retrouve pas.
* je vous invite à réviser les différentes étapes

###### Revue de code

* Activité traditionnelle de contrôle qualité dont le but principal est la détection d’erreurs.

* Liste de vérification (checklist)
>> Validité des résultats, la gestion des erreurs, la performance, la sécurité, la lisibilité, la documentation, …

###### TDD (Test-Driven Development)

* Test d'abord, code ensuite

* red, green, refactor

###### Avantage : 
- Première réflexion sur l'utilisation de la classe
- Grande banque de tests
- Meilleure maintenabilité
- Favorise le refactoring
- Design for Testability


### Tests fonctionnels

* Tests fonctionnels = tests effectués sur un logiciel en exécution 

* Ils vérifient la fiabilité des résultats 

* Pendant le développement on devrait lancer le logiciel pour voir si tout fonctionne

* Plus on le lance meilleur sera la code et notre productivité.


### Refactoring

###### refactoriser c'est : 
 - Réécrire
 - Retravailler
 - Refaire le design
 - Refaire l'architecture
 - Modifier du code existant, sans modifier la  fonctionnalité

 ###### But : 
 - Rendre le code plus lisible, plus clair, faciliter les changements, faciliter l’entretien du système

 ###### Quand ?
 - Chaque fois que vous voyez une meilleure façon de faire les choses («Meilleur ici» signifie rendre le code plus facile à comprendre et à modifier à l'avenir)

 * Préalable : Des tests unitaires

 ###### Comment ? : 
 - Ne pas faire de refactoring et ajouter de la  fonctionnalité en même temps.
 - Faites un petit changement


### IC/ID

#### Intégration continue

* On veut détecter les erreurs le plus rapidement possible.

* On veut intégrer son travail avec le travail des autres le plus rapidement possible et le plus  souvent possible

* On veut faire un build de l'application aussi tôt que possible

* Chaque commit devrait laisser le logiciel dans un état stable

###### Prérequis : 
- Gestionnaire de sources (un repo identifié comme étant le principal)
- Build automatisé 
- Suite de tests complète

###### Consignes:
-  Tous les commits dans «master»
- Garder le processus rapide (Un processus étagé (mvn) peut aider).
- Corriger les erreurs: **Toute l’équipe doit être avisée** lorsque le build est cassé. La correction du build est une tâche prioritaire. 


###### Avantages :
- Évite les erreurs croisées
- Permet de détecter les erreurs plus tôt ou plus rapidement
- Encourage l’écriture de tests
- Assure qu'une version du système est toujours « complète »
- Encourage les bonnes pratiques de développement.

#### Livraison continue

* S'assurer que le système en développement arrive rapidement dans un état livrable. Non pas au sens de «complété» mais plutôt «peut être exécuté dans l'environnement de test ». 

* La Livraison continue fait en sorte que le système est automatiquement testé dans un environnement «proche» de l'environnement de production. 

#### Déploiement continue

* Automatiser le déploiement en prod.

* Cette dernière tâche nécessiter une collaboration entre l’équipe de développement et l’équipes des opérations

* Cette collaboration n’est pas toujours bonne et cause souvent beaucoup de délai.

#### DevOps

* Un changement de mentalité qui vise à concilier ces deux besoins d'affaires. 

###### But : 
- Accélérer les déploiements
- Réduire le Time-to-Market (le temps d'arrivée d'une fonctionnalité sur le marché)

###### Le DevOps répose sur 5 piliers qui sont :
- La Culture
- L'automatisation
- Le lean
- La mesure
- Le share ou partage


# Partie 2


###### Q1
Sachant que toute manipulation du code peut entraîner l'injection d'un bogue, comment le refactoring peut-il être bénéfique pour un logiciel?


###### Q2 
Comment l'injection de dépendances permet-elle de faciliter la mise en test d'une classe? Donnez un exemple


###### Q3 
Le déploiement quotidien est une des pratiques d'eXtreme Programming les plus difficiles à mettre en place. Quels sont les prérequis nécessaires afin de pouvoir appliquer cette pratique?


###### Q4
Lors de la rédaction de tests unitaires, quelle est l'utilité d'un mock object?


###### Q5
Quelles sont les responsabilités du gestionnaire de sources et de l'outil de build dans la pratique de l'intégration continue?


###### Q6
Pourquoi la méthode TDD (Test-Driven Development) favorise-t-elle l'amélioration successive du code source?


###### Q7
Vous devez modifier une fonction de plus de 500 lignes de code. Avant la modification, vous aimeriez y faire un peu de refactoring. La fonction ne possède aucune couverture de test. Expliquez votre démarche afin de faire votre refactoring.


###### Q8
Quels sont les conséquences, positives ou négatives, de l'adoption du Test-Driven Development sur la productivité des développeurs et la maintenabilité des logiciels?


###### Q9
Identifier deux situations dans lesquelles il serait pertinent de récourir aux mocks objects. Dans chaque cas, donnez un exemple.


###### Q10
Quel lien y a-t-il entre le style et la maintenabilité ?



# Partie 3

###### Q1
À quoi sert l’analyseur statique Checkstyle?
    * 1.	À avertir le programmeur lorsque des mauvaises pratiques de codage sont détectées
    * 2.	À avertir le programmeur lorsque les normes de codifications ne sont pas respectées
    * 3.	À avertir le programmeur lorsque des bogues possibles sont détectés
    * 4.	À avertir le programmeur lorsque les règles de mise en forme CSS sont erronées
    * 5.	Aucune de ces réponses


###### Q2
En Java, où est utilisé le type de casse PascalCase?
    * 1.	Dans le nom des constantes
    * 2.	Dans le nom des classes
    * 3.	Dans le nom des méthodes publiques
    * 4.	Dans le nom des variables d'instance publiques
    * 5.	Aucune de ces réponses

###### Q3
Quelle est la différence principale entre une branche et un tag?
   * A.	La valeur du tag ne peut pas changer
   * B.	On ne peut pas faire de pull request sur une branche
   * C.	La branche ne peut pas être fusionnée avec une autre branche
   * D.	Le tag ne peut pas être supprimé
   * E.	On ne peut pas faire un « git log » sur un tag
    
###### Q4
Quelle est la principale différence entre l’intégration continue et le déploiement continu?
    * A.	À la fin de la tâche de déploiement continu, le système est livré en production
    * B.	L’intégration continue utilise un gestionnaire de sources distribué
    * C.	Le processus de déploiement continu implique l’exécution des tests
    * D.	Le déploiement continu ne fait pas d’intégration
    * E.	Dans l’intégration continue, les tests sont automatisés
    
###### Q5
Vous voulez écrire un test unitaire sur une méthode qui établie une connexion avec un serveur de base de données et lui envoie des requêtes. Quelles techniques allez-vous employer?
    * A.	Injection de dépendance et mock object
    * B.	Test de caractérisation
    * C.	Injection de dépendance et test de caractérisation
    * D.	Encapsulation de la base de données
    * E.	Abandonner cette idée



----

