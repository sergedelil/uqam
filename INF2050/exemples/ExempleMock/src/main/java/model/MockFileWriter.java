package model;

import java.io.IOException;

public class MockFileWriter extends FileWriterWrapper {

    private String writtenData = "";

    public MockFileWriter(){
        super(null);
    }

    @Override
    public void write(String msg) throws IOException {
        writtenData = writtenData + msg;
    }

    public String getWrittenData(){
        return writtenData;
    }
}
