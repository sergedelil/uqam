package model;

import java.io.IOException;

import static java.lang.System.exit;

public class Utils {

    public static void main(String [] args ) {

        final int NUM_ARGUMENTS = 2;
        if (args.length < NUM_ARGUMENTS) {
            System.out.println("Invalid arguments");
            exit(1);
        }
        FileWriterWrapper writer = new FileWriterWrapper(null);
        writeOnDisk(writer, "INF2050-UQAM");
    }

    public static void writeOnDisk(FileWriterWrapper writer, String msg){

        try {
            writer.write(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
