package model;

import java.io.FileWriter;
import java.io.IOException;

public class FileWriterWrapper {

    private FileWriter writer;

    public FileWriterWrapper(FileWriter writer) {
        this.writer = writer;
    }

    public void write(String toWrite) throws IOException {
        writer.write(toWrite);
    }
}
