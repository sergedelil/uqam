package exemple.junit;


public class Monnaie {

    private int valeur = 0;

    public Monnaie (String valeur) throws IllegalArgumentException  {
        try {
            this.valeur = Integer.parseInt(valeur.trim());
        }catch (Exception e){
            throw new IllegalArgumentException();
        }
    }
    public Monnaie(){}

    @Override
    public String toString(){

        return this.valeur + "$";
    }


    public String additionner (Monnaie autre){

        this.valeur += autre.valeur;
        return this.toString();
    }

    public String soustraire (Monnaie autre) throws OperationInvalideException {

        verifierOperandes(autre);
        this.valeur -= autre.valeur;
        return this.toString();
    }

    public String multiplier (Monnaie autre){

        this.valeur = this.valeur * autre.valeur;
        return this.toString();
    }

    public String pourcent (int taux){
        // to be implemented;
        return "";
    }

    private void verifierOperandes(Monnaie autre) throws OperationInvalideException {

        if ( this.valeur < autre.valeur ) {

            throw new OperationInvalideException("Opération impossible : "
                    + this.valeur + " - " + autre.valeur);
        }
    }
}
