package exemple.junit;

public class Main {

    public static void main(String[] args) {
        Monnaie quatre = new Monnaie("4");
        System.out.println(" 4$ x 4$ = "+ quatre.multiplier(quatre));
    }
}
