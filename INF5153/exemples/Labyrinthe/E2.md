
## Modélisation du jeu Labyrinthe

On modélise ici le jeu [labyrinthe](https://www.regledujeu.fr/labyrinthe/) d’un point de vue objet. 

_But du jeu : Dans un labyrinthe enchanté, les joueurs partent à la chasse aux objets et aux créatures magiques. Chacun cherche à se frayer un chemin jusqu’à eux en faisant coulisser astucieusement les couloirs. Le premier joueur à découvrir tous ses secrets et à revenir à son point de départ remporte cette passionnante chasse aux trésors._

_Contenu : 1 plateau de jeu, 34 plaques Couloir, 24 cartes Objectif, 4 pions._

* Vidéo d'illustration du jeu [ici](https://www.youtube.com/watch?v=8W1CjKRnAng)


#### Première étatpe : retrouver les classes principales

L’une des façons simples de retrouver des classes est d’utiliser une classe pour représenter chaque élément physique du jeu, c’est à dire le Plateau, les plaques Couloir, les cartes Objectif, les Pions, et bien sûr le Jeu lui même.

![](images/classe1.png)

On utilise maintenant les relations entre classes et les multiplicités pour expliciter les dépendances entre classes.

![](images/classe2.png)


Si on regarde bien le plateau de jeu, on peut noter qu’il contient 16 couloirs fixes. Le plateau contient 49 cases. 16 couloirs fixes + 34 couloirs mobiles correspondent à 50 couloirs pour 49 cases. Il y a toujours un couloir libre durant le jeu.
On peut raffiner un petit peu notre conception en rajoutant les couloirs fixes.


![](images/classe3.png)

Une conception alternative serait d’avoir des couloirs ayant une orientation sans possibilité de la changer, et d’avoir une sous classe ajoutant une nouvelle fonctionnalité, le changement de l’orientation.


![](images/classe4.png)


On note de plus que les cartes objectif correspondent à des dessins qui se situent sur des couloirs. Il y a donc une relation spécifique entre une carte objectif et un couloir.


![](images/classe5.png)


#### Les règles du jeu

##### Préparation

_Mélanger les plaques face cachée, puis les placer sur les emplacements libres du plateau pour créer un labyrinthe aléatoire. La plaque supplémentaire servira à faire coulisser les couloirs du labyrinthe. Mélanger à leur tour les 24 cartes Objectif face cachée. En distribuer le même nombre à chaque joueur. Chacun les empile devant lui sans les regarder. Chaque joueur choisit ensuite un pion qu’il place sur sa case Départ au coin du plateau de la couleur correspondante._


Une des étapes du jeu est la préparation du jeu. C’est une opération de nombre classe jeu. De plus, on note qu’un pion à une position initiale fixe spécifique à sa couleur. 

On ne veut pas décider pour l’instant de la façon de gérer la position d’un couloir (numérotation 1 à 49, coordonnées). On crée une abstraction Position pour cela. De plus, la couleur permet de désigner un
pion particulier dans un monde physique. Ce n’est pas une caractéristique du pion lui même.


![](images/classe6.png)


On retrouve maintenant la notion de Joueur. On note aussi que les objectifs sont empilés.


![](images/classe7.png)


#### Déroulement de la partie

_Chaque joueur regarde secrètement la carte supérieure de sa pile. Le plus jeune joueur commence. La partie se poursuit dans le sens des aiguilles d’une montre._

On a besoin de l’âge des joueurs pour déterminer le premier joueur.

![](images/classe8.png)


_À son tour de jeu, le joueur doit essayer d’atteindre la plaque représentant le même dessin que celui sur la carte supérieure de sa pile. Pour cela il commence toujours par faire coulisser une rangée ou une colonne du labyrinthe en insérant la plaque supplémentaire du bord vers l’intérieur du plateau, puis il déplace son pion.

Ainsi, un tour se compose toujours de deux phases :

1. Modification des couloirs (Introduction de la carte couloir supplémentaire)
2. Déplacement du pion_


On peut représenter avec un diagramme de séquence le déroulement d’un tour :

![](images/classe9.png)


On voit apparaître les premières opérations sur le plateau et sur le pion. On doit modifier les couloirs par insertion d’une carte couloir. On doit pouvoir déplacer un pion. Un pion aura donc une position courante.


![](images/classe10.png)


##### Phase 1 : Modification des couloirs

_• 12 flèches sont dessinées en bordure de plateau. Elles indiquent les rangées et colonnes où peut être insérée la plaque supplémentaire pour modifier les couloirs du labyrinthe._


On a besoin d’identifier l’un des 12 endroits ou il est possible d’insérer le couloir libre. On pourrait utiliser un entier de 1 à 12. Mais on ne pourrait pas facilement vérifier que c’est bien une des 12 positions qu’il s’agit. Ici, on décide d’utiliser une énumération en préfixant chaque position de la première lettre de son orientation et un chiffre de 1 à 3. On peut raffiner notre méthode modifierCouloir().


![](images/classe11.png)


_• Quand vient son tour, le joueur choisit l’une de ces rangées ou colonnes et pousse la plaque supplémentaire vers l’intérieur du plateau jusqu’à ce qu’une nouvelle plaque soit expulsée à l’opposé. La plaque expulsée reste au bord du plateau jusqu’à ce qu’elle soit réintroduite à un autre endroit par le joueur suivant._

L’introduction du couloir libre dans la rangée permet de libérer un nouveau couloir. On doit récupérer ce couloir, et le stocker. Il s’agit d’un attribut de notre jeu.


![](images/classe12.png)


_• Ce dernier n’a cependant pas le droit de réintroduire la plaque Couloir à l’endroit d’où elle vient d’être expulsée !_

Il s’agit d’une contrainte métier. Il faudra garder en mémoire de quelle position vient le couloir libre. On peut créer un nouvel attribut. 

#### Question 2:

Avec cette nouvelle contrainte métier, Le joueur doit-il continuer d'agir directement sur le plateau ? Sinon, qui devrait avoir cette responsabilité et pourquoi ?

 - Proposez un diagramme de séquence qui l'illustre.
