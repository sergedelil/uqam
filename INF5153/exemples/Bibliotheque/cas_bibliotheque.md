### Description : 

On souhaite informatiser une bibliothèque universitaire et on fait l’hypothèse qu’il n’y a pas déjà de système existant ou qu’il est tellement mauvais qu’on veut reprendre l’analyse et la conception depuis le départ. Cette bibliothèque propose aux étudiant.e.s de l’Université des livres et des revues à emprunter. Si un document n’est pas disponible, il est possible de le réserver. 

Chaque document est identifié par un numéro unique. Un livre est caractérisé par un titre, un nom d’éditeur et un ou plusieurs auteur(s). Une revue possède en outre un numéro de volume. Un document peut être emprunté ou consulté sur place. On ne peut pas emprunter plus de 3 documents en même temps, et la durée maximum d’un prêt est de 15 jours pour un livres et 7 jours pour une revue.

Les étudiant.e.s sont identifiés par leur code permanent, un nom, un prénom et une adresse. Ils peuvent consulter sur place, effectuer des recherches de documents par titre ou nom d'auteur en utilisant le système d'information de la bibliothèque.

Chaque document emprunté par un•e étudiant.e est connu du système d’information de la bibliothèque : l’étudiant.e donne le livre à emprunter à un.e employé.e à la caisse de sortie, avec sa carte UQAM. Pour rendre un livre, on le place simplement sur des étagères spécifiques à la caisse et régulièrement, les bibliothécaires utilisent le système pour remettre le livre dans le fond documentaire de la bibliothèque, puis physiquement dans la bonne étagère de présentation.

### Taches :

  1. Identification des concepts du domaine d'affaire
  2. Identification des responsabilités
  3. Attribution des responsabilités
