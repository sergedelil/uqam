
## Harry Potter

Voici un petit projet pour modéliser un problème commun lorsqu'on réalise la conception des systèmes de vente.

#### Spécifications

on va travailler ensemble sur le problème suivant : [kataPotter](https://codingdojo.org/kata/Potter/)

En gros, les 5 premiers livres de la saga Harry Potter sont à vendre dans une librairie. Chaque livre coûte 8$. La librairie applique certaines politiques de rabais en fonction du panier du consommateur (total des achats qui sont faits) :

* 2 livres différents : 5%
* 3 livres différents : 10%
* 4 livres différents : 20% 
* 5 livres différents : 25%

Le rabais ne s’applique que sur les livres concernées par la politique.


![](images/semaine1_hp_calcul.png)

Attention par contre, un rabais peut s'appliquer plus d'une fois sur la même liste d'articles!

![](images/semaine1_hp_piege.png)

#### À faire

- Listez les entités présentes dans ce problème
- Proposez un diagramme entités/rélations (diagramme des classes)
- Proposez un diagramme de séquence.

### Remarque :

Prendre conscience qu'un problème qui semble complexe peut devenir simple à résoudre lorsqu'une bonne structure est mise en place.